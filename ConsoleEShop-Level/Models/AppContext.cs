﻿using System.Collections.Generic;

namespace ConsoleEShop_Level.Models
{
    public class AppContext
    {
        public virtual IDictionary<int, Product> Products { get; set; }
        public virtual IDictionary<int, Order> Orders { get; set; }
        public virtual IDictionary<int, User> Users { get; set; }

        public AppContext()
        {
            Products = new Dictionary<int, Product>();
            Orders = new Dictionary<int, Order>();
            Users = new Dictionary<int, User>
            {
                { 1, 
                    new User
                    {
                        Role = Roles.Admin,
                        Login = "admin",
                        Password = "admin"
                    }
                } 
            };
        }
    }
}
