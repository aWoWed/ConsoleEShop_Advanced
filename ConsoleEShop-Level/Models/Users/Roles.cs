﻿namespace ConsoleEShop_Level.Models
{
    public enum Roles
    {
        Guest,
        User,
        Admin
    }
}
