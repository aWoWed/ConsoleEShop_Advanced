using System.Collections.Generic;
using ConsoleEShop_Level.Models;
using ConsoleEShop_Level.Services;
using Moq;


namespace ConsoleEShop_Level.Repositories
{
    public class DataManager
    {
        private readonly Mock<AppContext> _contextMock;
        private OrdersRepository _orderRepository;
        private ProductsRepository _productRepository;
        private UsersRepository _userRepository;
        private readonly ServiceManager _serviceManager = new ServiceManager();

        private static readonly object Locker = new object();
        private static DataManager _instance;
        
        public static DataManager Instance
        {
            get
            {
                lock (Locker)
                {
                    return _instance ??= new DataManager();
                }
            }
        }

        public IUsersRepository Users {
            get
            {
                return _userRepository ??= new UsersRepository(_contextMock.Object, _serviceManager);
            }
        }
        public IProductsRepository Products
        {
            get
            {
                return _productRepository ??= new ProductsRepository(_contextMock.Object, _serviceManager);
            }
        }
        public IOrdersRepository Orders {
            get
            {
                return _orderRepository ??= new OrdersRepository(_contextMock.Object, _serviceManager);
            }
        }

        public DataManager()
        {
            _contextMock = new Mock<AppContext>();
            _contextMock.Setup(context => context.Users).Returns(new Dictionary<int, User>());
            _contextMock.Setup(context => context.Products).Returns(new Dictionary<int, Product>());
            _contextMock.Setup(context => context.Orders).Returns(new Dictionary<int, Order>());
        }
    }
}
