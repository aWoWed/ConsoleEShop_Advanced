﻿using ConsoleEShop_Level.Controllers;
using ConsoleEShop_Level.Repositories;
using ConsoleEShop_Level.Views;

namespace ConsoleEShop_Level
{
    class Program
    {
        static void Main(string[] args)
        {
            var console = new ConsoleView();

            var dataManager = new DataManager();

            var controller = new MainController(console, dataManager);

            controller.Start();
        }
    }
}
